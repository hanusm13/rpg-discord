
import configparser
import asyncio
import logging
import re
import random

import discord

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

config = configparser.SafeConfigParser()
config.read("config.ini")

client = discord.Client()
admins = []
strict_channels = []

HELP_TEXT = """RPG-bot help text

available commands (more or less):

.help -- this text
.hello -- simple ping command
.id -- get user and channel id
.clear -- clear history (admins only (i hope))

.roll <dice> <target>:
rolls a dice
- dice is in format <count>d<dice>(+|-)<delta>
  everything that's not <dice> should be optional
  if no parameters are specified, rolls {default_dice}
- target needs to be specified as integer with over/under specifier
  before or after. Valid over/under specifier are: +,-,<,>

""".format(default_dice=config.get("Discord", "default_dice"))
@client.event
async def on_ready():
    logger.info("Discord client ready")
    logger.info("Client name: {} id: {}".format(client.user.name, client.user.id))
    logger.info("Changing presence")
    await client.change_presence(game=discord.Game(name="RPG"))
    logger.debug("Presence changed")

@client.event
async def on_message(message):
    if message.content.startswith("."):
        logger.info("CMD: {}: {}".format(message.author.name, message.content))
    else:
        return

    if message.content.startswith(".help"):
        await client.send_message(message.channel, HELP_TEXT)
        await client.delete_message(message)

    if message.content.startswith(".hello"):
        author = message.author
        nick = author.nick if author.nick is not None else author.name
        await client.send_message(message.channel, "Hello {}!".format(nick))
    
    if message.content.startswith(".id"):
        await client.delete_message(message)
        if message.channel.id not in strict_channels or message.author.id in admins:
            await client.send_message(message.channel, "{}\nChannel id: {}\nUser id: {}".format(message.author.name, message.channel.id, message.author.id))

    if message.content.startswith(".boteudelejzamelingebru"):
        await client.send_message(message.channel, "Nope, i'm out.")
    
    if message.content.startswith(".clear"):
        await client.delete_message(message)
        if message.author.id in admins:
            logger.info("Clearing the history")
            o = []
            async for m in client.logs_from(message.channel):
                o.append(m)
            await client.delete_messages(o)
        else:
            if message.channel.id not in strict_channels:
                await client.send_message(message.channel, "Permission denied (channel clear, user {})".format(message.author.name))
    
    if message.content.startswith(".roll"):
        cmd = message.content.split()
        success = None

        if len(cmd) == 1:
            dice = config.get("Discord", "default_dice")
        else:
            dice = cmd[1]

        tmp_msg = await client.send_message(message.channel, "Rolling for {} on {}".format(get_nick(message.author), dice))
        await client.delete_message(message)

        result = roll(dice)
        if result is None:
            await client.edit_message(tmp_msg, "Couldn't parse dice {} for {}".format(dice, get_nick(message.author)))
            return

        if len(cmd) >= 3:
            target = cmd[2]
            target_int = target
            target_ou = 1
            if target[0] in "<-":
                target_ou = -1
                target_int = target[1:]
            elif target[-1] in ">-":
                target_ou = -1
                target_int = target[:-1]
            elif target[0] in ">+":
                target_ou = 1
                target_int = target[1:]
            elif target[-1] in "<+":
                target_ou = 1
                target_int = target[:-1]

            try:
                target_int = int(target_int)
            except ValueError:
                target_ou = False
                logger.warning("Couldn't parse target '{}'".format(target))
                await client.edit_message(tmp_msg, "Couldn't parse target {} for {}".format(target, get_nick(message.author)))
            
            if target_ou:
                success = result * target_ou >= target_int * target_ou
                delta = abs(result-target_int)
        
        if success is not None:
            msg = "{} {} rolled {} on {} against {} ({})".format(
                "\N{WHITE HEAVY CHECK MARK}" if success else "\N{CROSS MARK}",
                get_nick(message.author),
                result, dice, target, delta)
        else:
            msg = "{} rolled {} on {}".format(
                get_nick(message.author),
                result, dice
            )
        await asyncio.sleep(2)
        await client.edit_message(tmp_msg, msg)

def get_nick(author):
    return author.nick if author.nick is not None else author.name

def roll(dice):
    DICE_RE = re.compile("^([0-9])*[dk]?([0-9]+)([+-][0-9]+)?$")

    match = DICE_RE.match(dice)
    if match is None:
        return None
    c, m, d = match.group(1,2,3)

    if c is None:
        c = 1
    else:
        c = int(c)
    if m is None:
        return None
    else:
        m = int(m)
    if d is None:
        d = 0
    else:
        d = int(d)
    
    result = 0

    for _ in range(c):
        result += random.randint(1, m)
    result += d

    return result

def main():
    global admins
    global strict_channels

    for section in config.sections():
        if section.startswith('U:'):
            if config.getboolean(section, "admin", fallback=False):
                logger.info("Found admin: {}".format(section))
                admins.append(section[2:])
        if section.startswith('C:'):
            if config.getboolean(section, "strict", fallback=False):
                logger.info("Found strict channel: {}".format(section))
                strict_channels.append(section[2:])


    token = config.get("Discord", "token")

    client.run(token)

if __name__ == "__main__":
    main()
